class Arbol {
    constructor() {
        this.nodoPadre; //para agregar un nodo padre
        this.nivel = 5; //se le pone el maximo nivel que tendra el arbol 
        this.busquedaElemento;//aqui se va a guardar el nodo donde se tiene el valor que se esta buscando
        this.buscarNodos = [];//aqui se van guardando los nombres de los nodos que coincidan con el nivel 5
    }

    agregarNodoPadre(valor){
        var nodo = new Nodo(null,"padre",valor);
        return nodo;
    }

    agregarNodo(nodoPadre,nombre,valor){
        var nodo = new Nodo(nodoPadre,nombre,valor);
        return nodo;
    }

    buscarValor(nombreElemento,valorElemento,nodo){
        camino = camino+nodo.valor;
        if(nodo.valor == valorElemento && nodo.nombre == nombreElemento)
        {
            this.busquedaElemento = nodo;
            totalCamino = camino;
            return this.busquedaElemento;
        }

        if(nodo.hI != undefined)
            this.buscarValor(nombreElemento,valorElemento,nodo.hI);//lo mismo que verificarNivelHijos (es como  un tipo foreach)

        if(nodo.hD != undefined)
            this.buscarValor(nombreElemento,valorElemento,nodo.hD);

        return this.busquedaElemento;//retorna el nodo que tenga el valor buscado
    }

}